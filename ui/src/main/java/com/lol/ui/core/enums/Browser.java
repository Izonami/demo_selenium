package com.lol.ui.core.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import java.util.Arrays;

@RequiredArgsConstructor
@Getter
@ToString
public enum Browser {

    CHROME("chrome"),
    CHROME_MOBILE("chrome_mobile"),
    CHROME_REMOTE("chrome_remote"),
    CHROME_REMOTE_MOBILE("chrome_remote_mobile"),

    FIREFOX("firefox"),
    FIREFOX_REMOTE("firefox_remote"),

    SAFARI("safari"),
    IE("ie"),
    OPERA("opera"),

    DEFAULT("chrome_remote");

    private final String name;

    public static Browser getValue(final String constant) {
        return Arrays.stream(Browser.values())
                .filter(e -> e.getName().equals(constant))
                .findFirst()
                .orElse(DEFAULT);
    }
}
