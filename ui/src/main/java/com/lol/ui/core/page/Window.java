package com.lol.ui.core.page;

import com.lol.ui.core.Calisto;
import org.openqa.selenium.WindowType;

import java.util.List;
import java.util.Optional;

import static com.lol.ui.core.Calisto.getWebDriver;

public interface Window {

    default void switchToNextWindow() {
        final var driver = getWebDriver();
        final String currentHandle = driver.getWindowHandle();
        final Optional<String> newTabHandle = driver.getWindowHandles()
                .stream()
                .filter(handle -> !handle.equals(currentHandle))
                .findFirst();
        newTabHandle.ifPresent(s -> driver.switchTo().window(s));
    }

    default void switchToFirstWindow() {
        final var driver = getWebDriver();
        final var windowHandles = List.copyOf(driver.getWindowHandles());
        driver.switchTo().window(windowHandles.get(0));
    }

    default void openInNewTab(final String url) {
        getWebDriver().switchTo().newWindow(WindowType.TAB).get(url);
    }

    default void openInNewWindow(final String url) {
        getWebDriver().switchTo().newWindow(WindowType.WINDOW).get(url);
    }

    default void closeAndSwitchToNextWindow() {
        if (getWebDriver().getWindowHandles().size() > 1) {
            Calisto.closeWindow();
        }
        switchToNextWindow();
    }

    default void closeAndSwitchToPrevWindow() {
        if (getWebDriver().getWindowHandles().size() > 1) {
            Calisto.closeWindow();
        }
        switchToFirstWindow();
    }
}
