package com.lol.ui.core.component.condition;

import com.lol.ui.core.Calisto;
import com.lol.ui.core.component.AbstractComponent;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.WebElement;

@RequiredArgsConstructor
public class Is {

    private final AbstractComponent component;

    public boolean displayed() {
        return Calisto.waitAction().isVisible(component);
    }

    public boolean invisible(final Object... args) {
        return Calisto.waitAction().isInvisible(component, args);
    }

    public boolean invisible(final WebElement webElement) {
        return Calisto.waitAction().isInvisible(component, webElement);
    }

    public boolean unclickable(final Object... args) {
        return Calisto.waitAction().isUnclickable(component, args);
    }

    public boolean animationFinished(final Object... args) {
        return Calisto.waitAction().isAnimationFinished(component, args);
    }

    public boolean containText(final String text) {
        return Calisto.waitAction().containText(component, text);
    }

    public boolean containTextFromAttribute(final String attribute, final String text) {
        return Calisto.waitAction().containTextFromAttribute(component, attribute, text);
    }

    public boolean containText(final String text, final Object... args) {
        return Calisto.waitAction().containText(component, text, args);
    }
}
