package com.lol.ui.core.component;

import com.lol.ui.core.ByCalisto;
import org.openqa.selenium.By;

import static java.util.Objects.nonNull;

public interface Component {

    By getBy();

    default By getBy(final Object... args) {
        if (nonNull(args) && (args.length > 0) && getBy() instanceof ByCalisto) {
            final ByCalisto byCalisto = (ByCalisto) getBy();
            return ByCalisto.xpathExpression(byCalisto.getDefaultXpathExpression(), args);
        }
        return getBy();
    }

    long getTimeout();
    String getDescription();
    String getErrorMsg();
}
