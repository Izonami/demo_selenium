package com.lol.ui.core.action;

import com.lol.ui.core.CalistoCondition;
import com.lol.ui.core.CalistoWait;
import com.lol.ui.core.component.Component;
import com.lol.ui.core.component.ElementCollection;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.time.Duration;
import java.util.List;
import java.util.regex.Pattern;

import static com.lol.ui.core.Calisto.getWebDriver;
import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;

@Slf4j
public final class WaitAction {

    //######################## Clickable ########################
    public WebElement shouldBeClickable(final Component component) {
        return createWait(component, "элемент должен быть кликабельным")
                .until(elementToBeClickable(component.getBy()));
    }

    public WebElement shouldBeClickable(final Component component, final WebElement webElement) {
        return createWait(component, "элемент должен быть кликабельным")
                .until(CalistoCondition.elementToBeClickable(webElement, component.getBy()));
    }

    //######################## Not Clickable ########################
    public boolean isUnclickable(final Component component, final Object... args) {
        return createWait(component, "элемент не должен быть кликабельным")
                .until(CalistoCondition.elementNotToBeClickable(component.getBy(args)));
    }

    //######################## Visible ########################
    public WebElement shouldBeVisible(final Component component, final WebElement webElement) {
        return createWait(component, "элемент должен отображаться")
                .until(CalistoCondition.visibilityOfElementLocated(webElement, component.getBy()));
    }

    public WebElement shouldBeVisible(final Component component, final Object... args) {
        return createWait(component, "элемент должен отображаться")
                .until(ExpectedConditions.visibilityOfElementLocated(component.getBy(args)));
    }

    public boolean isVisible(final Component component) {
        return createWait(component, "элемент должен быть видим")
                .until(CalistoCondition.visibilityOfElementLocated(component.getBy()));
    }

    public void frameShouldBeVisible(final int frame) {
        createWait("Фрейм не загрузился")
                .until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(frame));
    }

    public void tabOpenCount(final int tabCount) {
        createWait("Таб не загрузился").until(ExpectedConditions.numberOfWindowsToBe(tabCount));
    }

    //######################## Not Visible ########################
    public boolean isInvisible(final Component component, final WebElement webElement) {
        return createWait(component, "элемент не должен быть видим")
                .until(CalistoCondition.invisibilityOfElementLocated(webElement, component.getBy()));
    }

    public boolean isInvisible(final Component component, final Object... args) {
        return createWait(component, "элемент не должен быть видим")
                .until(ExpectedConditions.invisibilityOfElementLocated(component.getBy(args)));
    }

    //######################## Exist ########################
    public WebElement shouldExist(final Component component) {
        return createWait(component, "элемент должен быть в DOM")
                .until(ExpectedConditions.presenceOfElementLocated(component.getBy()));
    }

    public WebElement shouldExist(final Component component, final WebElement webElement) {
        return createWait(component, "элемент должен быть в DOM")
                .until(CalistoCondition.presentsOfElementLocated(webElement, component.getBy()));
    }

    public List<WebElement> isElementsExist(final Component component) {
        return createWait(component, "коллекция элементов должна быть видима")
                .until(CalistoCondition.elementCollectionPresent(component.getBy()));
    }

    public List<WebElement> isElementsExist(final Component component, final WebElement webElement) {
        return createWait(component, "коллекция элементов должна быть видима")
                .until(CalistoCondition.elementCollectionPresent(component.getBy(), webElement));
    }

    //######################## Not Exist ########################
    public boolean isElementsShouldNotBeExist(final ElementCollection component) {
        return createWait(component, "коллекции элементов не должно быть")
                .until(CalistoCondition.elementCollectionNotPresent(component.getBy()));
    }

    //######################## Not Animated ########################
    public boolean isAnimationFinished(final Component component, final Object... args) {
        return createWait(component, "анимация у элемента должна закончиться")
                .until(CalistoCondition.steadinessOfElementLocated(component.getBy(args)));
    }

    //######################## CheckState ########################
    public void elementSelectCheckboxState(final WebElement element, final boolean selected) {
        createWait("Состояние чекбокса, отличается от ожидаемого")
                .until(CalistoCondition.elementSelectCheckboxState(element, selected));
    }

    //######################## Url equals ########################
    public boolean isUrlEquals(final String url) {
        return createWait(String.format("Текущая страница: %s отличается от ожидаемой: %s", getWebDriver().getCurrentUrl(), url))
                .until(ExpectedConditions.urlToBe(url));
    }

    //######################## Url Contains ########################
    public boolean isUrlContains(final String url) {
        return createWait(String.format("Текущая страница: %s не содержит ожидаемого url: %s", getWebDriver().getCurrentUrl(), url))
                .until(ExpectedConditions.urlContains(url));
    }

    //######################## Contains Text ########################
    public boolean textMatches(final Component component, final Pattern pattern, final Object... args) {
        return createWait(component, String.format("текст элемента должен соответствовать паттерну: %s", pattern))
                .until(ExpectedConditions.textMatches(component.getBy(args), pattern));
    }

    public boolean containText(final Component component, final String text, final Object... args) {
        return createWait(component, String.format("должен присутствовать текст: %s", text))
                .until(ExpectedConditions.textToBePresentInElementLocated(component.getBy(args), text));
    }

    public boolean containTextFromAttribute(final Component component, final String attribute, final String text, final Object... args) {
        return createWait(component, String.format("должен присутствовать текст: %s в атрибуте %s", text, attribute))
                .until(CalistoCondition.textToBePresentInAttributeLocated(component.getBy(args), attribute, text));
    }

    public void fillField(final Component component, final String data) {
        createWait("Текущее содержимое поля отличается от ожидаемого")
                .until(CalistoCondition.keysSendCondition(component.getBy(), data));
    }

    public boolean isElementCollectionSizeEqual(final ElementCollection collection, final int size) {
        return createWait(collection, String.format("Кол-во элементов в коллекции: %s не совпадает с ожидаемым: %d", collection.elementCount(), size))
                .until((ExpectedCondition<Boolean>) wb -> collection.elementCount() == size);
    }

    //######################## Attribute ########################
    public boolean hasAttributeWithValue(final Component component, final String attributeName, final String attributeValue) {
        return createWait(component, "элемент должен иметь указанный аттрибут со значением")
                .until(ExpectedConditions.attributeToBe(component.getBy(), attributeName, attributeValue));
    }

    private CalistoWait<WebDriver> createWait(final String errorConditionMsg) {
        return new CalistoWait<>(getWebDriver())
                .withMessage(errorConditionMsg)
                .ignoring(NoSuchElementException.class)
                .ignoring(ElementClickInterceptedException.class)
                .ignoring(NotFoundException.class);
    }

    private CalistoWait<WebDriver> createWait(final Component component, final String errorConditionMsg) {
        return new CalistoWait<>(getWebDriver())
                .withTimeout(Duration.ofSeconds(component.getTimeout()))
                .withMessage(component.getErrorMsg() + errorConditionMsg)
                .ignoring(NoSuchElementException.class)
                .ignoring(ElementClickInterceptedException.class)
                .ignoring(NotFoundException.class);
    }
}
