package com.lol.ui.core.provider;

import com.lol.ui.core.config.BrowserProperties;
import com.lol.ui.core.config.WaitProperties;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.devtools.DevTools;
import org.openqa.selenium.devtools.HasDevTools;
import org.openqa.selenium.devtools.v131.network.Network;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.http.ClientConfig;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.safari.SafariOptions;

import java.net.URI;
import java.time.Duration;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;

@Slf4j
public abstract class AbstractBrowserProvider {

    @Getter
    protected WebDriver driver;
    @Getter
    protected DevTools devTools;
    private final ClientConfig clientConfig = ClientConfig.defaultConfig().withRetries();

    public abstract void createDriver(final String version);

    protected void createRemoteDriver(final Capabilities capabilities) {
        try {
            final var url = URI.create(BrowserProperties.REMOTE_URL).toURL();
            clientConfig.connectionTimeout(Duration.ofSeconds(WaitProperties.REMOTE_DRIVER_CONNECTION_TIMEOUT))
                    .readTimeout(Duration.ofMinutes(WaitProperties.REMOTE_DRIVER_READ_TIMEOUT));
            this.driver = RemoteWebDriver.builder()
                    .address(url)
                    .augmentUsing(new Augmenter())
                    .oneOf(capabilities)
                    .config(clientConfig)
                    .build();
            applyOptions();
            ((RemoteWebDriver) driver).setFileDetector(new LocalFileDetector());
        } catch (Exception e) {
            log.error("Protocol exception ", e);
        }
    }

    protected void createLocalChromeDriver(final ChromeOptions capabilities) {
        createLocalDriver(capabilities, ChromeDriver::new);
    }

    protected void createLocalSafariDriver(final SafariOptions capabilities) {
        createLocalDriver(capabilities, SafariDriver::new);
    }

    protected void createLocalFirefoxDriver(final FirefoxOptions capabilities) {
        createLocalDriver(capabilities, FirefoxDriver::new);
    }

    protected void createLocalIEDriver(final InternetExplorerOptions capabilities) {
        createLocalDriver(capabilities, InternetExplorerDriver::new);
    }

    private <T extends WebDriver, O extends Capabilities> void createLocalDriver(
            O capabilities, Function<O, T> driverFactory) {
        this.driver = Objects.nonNull(capabilities) ? driverFactory.apply(capabilities) : driverFactory.apply(null);
        applyOptions();
    }

    private void applyOptions() {
        initDevTools();
    }

    private void initDevTools() {
        this.devTools = ((HasDevTools) driver).getDevTools();
        this.devTools.createSession();
        this.devTools.send(Network.enable(Optional.empty(), Optional.empty(), Optional.empty()));
    }
}
