package com.lol.ui.core.config;

import com.lol.core.config.Config;

import java.util.List;

public final class BrowserProperties {

    public static final String NAME = "browser";

    @Config(configName = NAME, fieldName = "defaultBrowser", defaultValue = "chrome_remote", env = "BROWSER")
    public static String BROWSER;
    @Config(configName = NAME, fieldName = "browserVersion", defaultValue = "latest", env = "BROWSER_VERSION")
    public static String BROWSER_VERSION;

    @Config(configName = NAME, fieldName = "remoteUrl", defaultValue = "remoteUrl")
    public static String REMOTE_URL;
    @Config(configName = NAME, fieldName = "video", defaultValue = "false")
    public static boolean VIDEO;
    @Config(configName = NAME, fieldName = "headless", defaultValue = "false", env = "HEADLESS")
    public static boolean HEADLESS;

    @Config(configName = NAME, fieldName = "ignoreSsl", defaultValue = "true")
    public static boolean IGNORE_SSL;

    @Config(configName = NAME, fieldName = "enableProfile", defaultValue = "false")
    public static boolean ENABLE_PROFILE;
    @Config(configName = NAME, fieldName = "profilePath", defaultValue = "")
    public static String PROFILE_PATH;
    @Config(configName = NAME, fieldName = "profileName", defaultValue = "Default")
    public static String PROFILE_NAME;

    @Config(configName = NAME, fieldName = "hostsEntries", defaultValue = "", env = "HOSTS")
    public static List<String> HOSTS_ENTRIES;
}
