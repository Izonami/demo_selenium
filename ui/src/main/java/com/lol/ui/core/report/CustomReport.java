package com.lol.ui.core.report;

import com.lol.ui.core.Calisto;
import com.lol.ui.core.cdp.CdpCookie;
import io.qameta.allure.Attachment;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.logging.LogType;

import java.util.Arrays;
import java.util.StringJoiner;

public final class CustomReport {

    @Attachment(value = "Браузерный лог", type = "text/plain")
    public static String addBrowserLog() {
        final var joiner = new StringJoiner("\n");
        Calisto.getLogs(LogType.BROWSER)
                .forEach(log -> joiner.add(log.getMessage()));

        return joiner.toString();
    }

    @Attachment(value = "Содержимое страницы", type = "text/html")
    public static String addSourcePage() {
        return Calisto.getSource();
    }

    @Attachment(value = "LocalStorage", type = "text/plain")
    public static String addLocalStorage() {
        final var joiner = new StringJoiner("\n");
        final String[] storage = Calisto.jsAction().getLocalStorage().split(",");
        Arrays.stream(storage).forEach(joiner::add);

        return joiner.toString();
    }

    /**
     * Создаём скриншот и добавляем его в Allure
     */
    @Attachment(value = "Скриншот с веб страницы", type = "image/jpg")
    public static byte[] takeScreenshot() {
        return ((TakesScreenshot) Calisto.getWebDriver()).getScreenshotAs(OutputType.BYTES);
    }


    @Attachment(value = "Куки браузера", type = "text/plain")
    public static String addCookieLog() {
        final var cookies = CdpCookie.getAllCookies();
        final int maxLineLength = cookies
                .stream()
                .map(org.openqa.selenium.devtools.v131.network.model.Cookie::getValue)
                .mapToInt(String::length)
                .max().orElse(0);
        final int valueColumnWidth = Math.min(Math.max(maxLineLength, 20), 120);
        final var delimiter = buildDelimiter(40, valueColumnWidth, 40);
        final var sb = new StringBuilder();

        sb.append(delimiter);
        sb.append(String.format("|%-40s|%-" + valueColumnWidth + "s|%-40s|%n", "Name", "Value", "Domain"));
        sb.append(delimiter);

        cookies.forEach(cookie -> appendCookieRow(sb, cookie, valueColumnWidth, delimiter));

        return sb.toString();
    }

    private static String buildDelimiter(final int nameWidth, final int valueWidth, final int domainWidth) {
        return "+" + "-".repeat(nameWidth) + "+" + "-".repeat(valueWidth) + "+" + "-".repeat(domainWidth) + "+\n";
    }

    private static void appendCookieRow(final StringBuilder sb,
                                        final org.openqa.selenium.devtools.v131.network.model.Cookie cookie,
                                        final int valueColumnWidth,
                                        final String delimiter) {
        final String wrappedValue = wrapText(cookie.getValue(), valueColumnWidth);
        final String[] valueLines = wrappedValue.split("\n");

        for (int i = 0; i < valueLines.length; i++) {
            sb.append(String.format("|%-40s|%-" + valueColumnWidth + "s|%-40s|%n",
                    i == 0 ? cookie.getName() : "",
                    valueLines[i],
                    i == 0 ? cookie.getDomain() : ""));
        }
        sb.append(delimiter);
    }

    private static String wrapText(String text, int maxLineLength) {
        return text.replaceAll(".+" + maxLineLength + "}(?=\\s|$)", "$0\n").strip();
    }
}
