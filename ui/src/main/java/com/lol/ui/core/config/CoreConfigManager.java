package com.lol.ui.core.config;

import com.lol.core.config.AbstractConfigManager;

public final class CoreConfigManager extends AbstractConfigManager {

    private static final String CONFIG_DIR = "config";

    @Override
    public void loadConfig() {
        ENGINE.loadConfig(BrowserProperties.class, BrowserProperties.NAME, CONFIG_DIR);
        ENGINE.loadConfig(WaitProperties.class, WaitProperties.NAME, CONFIG_DIR);
    }

    private CoreConfigManager() {}

    public static CoreConfigManager getInstance() {
        return Singleton.INSTANCE;
    }

    private static class Singleton {
        private static final CoreConfigManager INSTANCE = new CoreConfigManager();
    }
}
