package com.lol.ui.core.page;

import com.lol.ui.core.Calisto;
import io.qameta.allure.Step;

import static org.testng.Assert.assertNotEquals;
import static org.testng.Assert.assertTrue;

public interface PageCheck {

    @Step("Проверка что находимся на странице '{0}'")
    default void checkPageUrl(final String url) {
        assertTrue(Calisto.waitAction().isUrlEquals(url), String.format("Адрес текущей страницы: '%s' отличается от ожидаемого: '%s'", Calisto.currentUrl(), url));
    }

    @Step("Проверка что страница '{0}' не открылась")
    default void checkForbiddenPageUrl(final String url) {
        assertNotEquals(Calisto.currentUrl(), url, "Текущая страница должна быть недоступна");
    }

    @Step("Проверка что страница содержит часть url '{0}'")
    default void checkPageContains(final String expectedUrl) {
        assertTrue(Calisto.waitAction().isUrlContains(expectedUrl),
                String.format("Адрес текущей страницы: '%s' не содержит ожидаемый фрагмент: '%s'", Calisto.currentUrl(), expectedUrl));
    }

    @Step("Ожидание загрузки страницы DOM и Network")
    default void waitPageLoad() {
        assertTrue(Calisto.jsAction().waitForDocumentReady(), "DOM не в статусе ready");
        assertTrue(Calisto.jsAction().checkPendingRequests(), "Запросы из нетворка остались в статусе pinding");
    }

    @Step("Проверяем, что на странице открылся фрейм")
    default void checkFrameOpened() {
        Calisto.waitAction().frameShouldBeVisible(0);
    }

    @Step("Проверка, что открылась вкладка")
    default void checkTabOpened() {
        Calisto.waitAction().tabOpenCount(2);
    }
}
