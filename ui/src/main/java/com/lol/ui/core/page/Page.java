package com.lol.ui.core.page;

import com.lol.ui.core.Calisto;
import com.lol.ui.core.cdp.CdpCookie;
import io.qameta.allure.Step;
import org.openqa.selenium.Cookie;

import java.util.List;

public interface Page extends PageCheck {
    
    String pageUrl();

    @Step("Обновить страницу")
    default void refresh() {
        Calisto.refresh();
    }

    @Step("Добавить куки {0}")
    default void addCookie(final Cookie cookie) {
        CdpCookie.addCookie(cookie);
    }

    @Step("Добавить список кук {0}")
    default void addCookies(final List<Cookie> cookies) {
        CdpCookie.addCookies(cookies);
    }

    @Step("Скролл страницы вверх")
    default void scrollUp() {
        Calisto.jsAction().scrollToTheTop();
    }

    @Step("Скролл страницы вниз")
    default void scrollDown() {
        Calisto.jsAction().scrollToTheBottom();
    }

    @Step("Подтвердить браузерный алерт")
    default void confirmBrowserAlert() {
        Calisto.alertConfirm();
    }

    @Step("Отклонить браузерный алерт")
    default void declineBrowserAlert() {
        Calisto.alertDismiss();
    }
}
