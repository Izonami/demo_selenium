package com.lol.ui.core.listener;

import com.lol.core.listener.AllureTestNgListener;
import com.lol.ui.core.Calisto;
import com.lol.ui.core.annotation.DoNotOpenBrowser;
import com.lol.ui.core.report.CustomReport;
import lombok.extern.slf4j.Slf4j;
import org.testng.IInvokedMethod;
import org.testng.ISuite;
import org.testng.ITestContext;
import org.testng.ITestResult;

import java.lang.reflect.Method;

@Slf4j
public class UiDefaultListener extends AllureTestNgListener {

    @Override
    public void beforeInvocation(IInvokedMethod method, ITestResult testResult) {
        super.beforeInvocation(method, testResult);
    }

    @Override
    public void onStart(ITestContext context) {
        super.onStart(context);
        //Тут может быть работа с TMS
    }

    @Override
    public void onTestFailure(ITestResult result) {
        super.onTestFailure(result);
        fireRetryTest(result);
    }

    @Override
    public void onFinish(ISuite suite) {
        //Тут может быть работа с TMS
        super.onFinish(suite);
    }

    protected void tearDown(final Method method, final ITestResult result) {
        if (method.isAnnotationPresent(DoNotOpenBrowser.class) || !Calisto.isAlive()) {
            return;
        }
        if (result.isSuccess()) {
            CustomReport.addBrowserLog();
            CustomReport.addCookieLog();
            CustomReport.takeScreenshot();
            CustomReport.addLocalStorage();
        }
        Calisto.closeBrowser();
    }
}
