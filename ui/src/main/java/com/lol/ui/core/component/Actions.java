package com.lol.ui.core.component;

import com.lol.core.function.TriConsumer;
import com.lol.ui.core.Calisto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import java.util.function.Consumer;

@RequiredArgsConstructor
@Slf4j
public final class Actions {

    private final AbstractComponent component;
    private final Consumer<WebElement> hover = (we) -> Calisto.action().moveToElement(we).perform();
    private final Consumer<WebElement> moveToElementAndClick = (we) -> Calisto.action().moveToElement(we).click().perform();
    private final Consumer<Keys> sendKeys = (key) -> Calisto.action().sendKeys(key).perform();
    private final TriConsumer<WebElement, Integer, Integer> clickWithOffset = (we, xOffset, yOffset) -> Calisto.action().moveToElement(we, xOffset, yOffset).click().perform();

    public void mouseOver() {
        log.debug("Element {} '{}' hover", component.getDescription(), component.getBy());
        hover.accept(component.getComponent());
    }

    public void sendKeys(final Keys keys) {
        log.debug("Send keys {} to element {}", keys, component.getDescription());
        sendKeys.accept(keys);
    }

    public void clickWithOffset(final int xOffset, final int yOffset) {
        log.debug("Click to element '{}' with offset x={}, y={}", component.getDescription(), xOffset, yOffset);
        clickWithOffset.accept(component.getComponent(), xOffset, yOffset);
    }

    /**
     * Наведение на элемент и клик через механизм Actions.MoveToElement()
     */
    public void moveToElementAndClick() {
        log.debug("Move to element and click {} '{}'", component.getDescription(), component.getBy());
        moveToElementAndClick.accept(component.getComponent());
    }
}
