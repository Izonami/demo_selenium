package com.lol.ui.core;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebElement;

import java.io.Serializable;
import java.util.List;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

@Slf4j
public final class ByCalisto extends By implements Serializable {

    private static final long serialVersionUID = 1L;

    @Getter
    private final String defaultXpathExpression;
    private final String xpathExpression;

    public ByCalisto(final String xpathExpression, final Object... args) {
        this.defaultXpathExpression = xpathExpression;
        if (nonNull(args) && (args.length > 0)) {
            this.xpathExpression = String.format(defaultXpathExpression, args);
            log.debug("Custom locator {}", this.xpathExpression);
        } else {
            this.xpathExpression = defaultXpathExpression;
        }
    }

    @Override
    public List<WebElement> findElements(SearchContext context) {
        return context.findElements(By.xpath(xpathExpression));
    }

    @Override
    public WebElement findElement(SearchContext context) {
        return context.findElement(By.xpath(xpathExpression));
    }

    public static By xpathExpression(final String name, final Object... args) {
        if (isNull(name))
            throw new IllegalArgumentException(
                    "Cannot find elements when name text is null.");
        return new ByCalisto(name, args);
    }

    @Override
    public String toString() {
        return "By.xpath: " + xpathExpression;
    }
}
