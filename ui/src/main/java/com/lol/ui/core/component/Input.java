package com.lol.ui.core.component;

import com.lol.ui.core.Calisto;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

@ToString(callSuper = true)
@Slf4j
public final class Input extends AbstractComponent {

    public Input(final By by, final String description) {
        super(by, description);
    }

    public Input(final By by, final long timeout, final String description) {
        super(by, timeout, description);
    }

    @Override
    public WebElement getComponent() {
        log.debug("getComponent {} with locator {}", getDescription(), getBy());
        return shouldBe().clickable();
    }

    /**
     * Очистка может не работать с полями у которых атрибут autocomplete
     *
     * @param data - заполняемое значение
     */
    public void fill(final String data) {
        final var component = getComponent();
        component.clear();
        log.debug("Fill {} with locator {} and data {}", getDescription(), getBy(), data);
        component.sendKeys(data);
    }

    /**
     * Чаще всего используется там где есть атрибут autocomplete или происходит форматирование введенного текста js'ом
     * под капотом через ожидания заполняет и сверяет, что введенный текст соответствует,
     * тому что введено, если ввод оказывается ошибочным, то стирает все в инпуте и пытается заполнить снова
     *
     * @param data    - заполняемое значение
     */
    public void fillField(final String data) {
        log.debug("Fill with wait and check {} with locator {} and data {}", getDescription(), getBy(), data);
        Calisto.waitAction().fillField(this, data);
    }

    public void click() {
        log.debug("Click into field {}", getDescription());
        getComponent().click();
    }

    public String getValue() {
        log.debug("Get value");
        return getComponent().getAttribute("value");
    }

    public String getText() {
        log.debug("Get text");
        return getComponent().getText();
    }
}
