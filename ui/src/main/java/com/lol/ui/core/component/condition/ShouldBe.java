package com.lol.ui.core.component.condition;

import com.lol.ui.core.Calisto;
import com.lol.ui.core.component.AbstractComponent;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.WebElement;

import java.util.List;

@RequiredArgsConstructor
public final class ShouldBe {

    private final AbstractComponent component;

    public WebElement visible(final Object... args) {
        return Calisto.waitAction().shouldBeVisible(component, args);
    }

    public WebElement elementExists() {
        return Calisto.waitAction().shouldExist(component);
    }

    public List<WebElement> elementsExists() {
        return Calisto.waitAction().isElementsExist(component);
    }

    public WebElement clickable() {
        return Calisto.waitAction().shouldBeClickable(component);
    }
}
