package com.lol.ui.core.provider.chrome;

import com.lol.core.enums.CiPipelineSource;
import com.lol.ui.core.config.BrowserProperties;
import com.lol.ui.core.config.WaitProperties;
import com.lol.ui.core.provider.AbstractBrowserProvider;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.chrome.ChromeOptions;

import java.time.Duration;
import java.util.Map;

public class AbstractChromeProvider extends AbstractBrowserProvider {

    protected ChromeOptions options = new ChromeOptions();

    @Override
    public void createDriver(String version) {
        if (BrowserProperties.ENABLE_PROFILE) {
            options.addArguments("--user-data-dir=" + BrowserProperties.PROFILE_PATH);
            options.addArguments("--profile-directory=" + BrowserProperties.PROFILE_NAME);
        }

        options.addArguments("--disable-extensions");
        options.addArguments("--disable-geolocation");
        options.addArguments("--no-sandbox");
        options.addArguments("--disable-notifications");
        options.addArguments("--disable-dev-shm-usage");
        options.addArguments("--disable-gpu");

        options.setExperimentalOption("prefs", Map.ofEntries(
                Map.entry("profile.default_content_settings.geolocation", 2),
                Map.entry("profile.managed_default_content_settings.geolocation", 2),
                Map.entry("credentials_enable_service", false),
                Map.entry("profile.password_manager_enabled", false),
                Map.entry("profile.default_content_settings.popups", 0),
                Map.entry("download.default_directory", getDownloadPath()),
                Map.entry("download.prompt_for_download", false),
                Map.entry("download.directory_upgrade", true),
                Map.entry("safebrowsing.enabled", false),
                //Если true, то pdf скачивается, если же false то открывается в Chrome PDF View плагине
                Map.entry("plugins.always_open_pdf_externally", false)
        ));
        options.setExperimentalOption("excludeSwitches", new String[]{"enable-automation"});

        options.setPageLoadStrategy(PageLoadStrategy.EAGER);
        options.setUnhandledPromptBehaviour(UnexpectedAlertBehaviour.ACCEPT);

        if (BrowserProperties.HEADLESS) options.addArguments("--headless=new");

        options.setBrowserVersion(version);
        options.setAcceptInsecureCerts(BrowserProperties.IGNORE_SSL);
        options.addArguments("window-size=1920,1080");
        options.setPageLoadTimeout(Duration.ofSeconds(WaitProperties.MAX_PAGE_LOAD_TIMEOUT));
        options.setScriptTimeout(Duration.ofSeconds(WaitProperties.MAX_SCRIPT_LOAD_TIMEOUT));
    }

    /**
     * @return - при локальном запуске строим путь до папки reforged/Downloads, если же запускаем в Moon то отдаем его директорию
     */
    private String getDownloadPath() {
        return CiPipelineSource.isLocal() ? System.getProperty("user.dir") + "/Downloads" : "/home/user/Downloads";
    }
}
