package com.lol.ui.core.component.condition;

import com.lol.ui.core.Calisto;
import com.lol.ui.core.component.AbstractComponent;
import com.lol.ui.core.component.ElementCollection;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.WebElement;

import java.util.regex.Pattern;

import static org.testng.Assert.assertTrue;

@RequiredArgsConstructor
public final class Should {

    private final AbstractComponent component;

    public void invisible(final Object... args) {
        assertTrue(Calisto.waitAction().isInvisible(component, args), String.format("Элемент '%s' должен быть невидимым", component.getDescription()));
    }

    public void invisible(final WebElement webElement) {
        assertTrue(Calisto.waitAction().isInvisible(component, webElement), "Элемент должен быть невидимым");
    }

    public void unclickable(final Object... args) {
        assertTrue(Calisto.waitAction().isUnclickable(component, args), "Элемент должен быть не кликабелен");
    }

    public void animationFinished(final Object... args) {
        assertTrue(Calisto.waitAction().isAnimationFinished(component, args), "Анимация должна быть завершена");
    }

    public void visible() {
        Calisto.waitAction().shouldBeVisible(component);
    }

    public void visible(final Object args) {
        Calisto.waitAction().shouldBeVisible(component, args);
    }

    public void clickable() {
        Calisto.waitAction().shouldBeClickable(component);
    }

    public void textContains(final String text) {
        assertTrue(Calisto.waitAction().containText(component, text),
                String.format("Текст элемента '%s' не содержит ожидаемого: '%s'", component.getBy(), text));
    }

    public void textContains(final String text, final String args) {
        assertTrue(Calisto.waitAction().containText(component, text, args),
                String.format("Текст элемента '%s' не содержит ожидаемого: '%s'", component.getBy(args), text));
    }

    public void textMatches(final Pattern pattern, final String args) {
        assertTrue(Calisto.waitAction().textMatches(component, pattern, args),
                String.format("Текст элемента '%s' не соответсвует паттерну: '%s'", component.getBy(args), pattern));
    }

    public void haveAttributeWithValue(final String attributeName, final String attributeValue) {
        assertTrue(Calisto.waitAction().hasAttributeWithValue(component, attributeName, attributeValue),
                String.format("Элемент: '%s' должен иметь аттрибут '%s' со значением '%s'", component.getDescription(), attributeName, attributeValue));
    }

    public void elementsNotExist() {
        assertTrue(Calisto.waitAction().isElementsShouldNotBeExist((ElementCollection) component), "Коллекции не должно быть");
    }
}
