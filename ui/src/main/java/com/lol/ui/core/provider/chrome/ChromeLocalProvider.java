package com.lol.ui.core.provider.chrome;

import io.github.bonigarcia.wdm.WebDriverManager;

public final class ChromeLocalProvider extends AbstractChromeProvider {

    @Override
    public void createDriver(final String version) {
        WebDriverManager.chromedriver().browserVersion(version).setup();
        super.createDriver(version);

        options.addArguments("--remote-allow-origins=*");

        createLocalChromeDriver(options);
    }
}
