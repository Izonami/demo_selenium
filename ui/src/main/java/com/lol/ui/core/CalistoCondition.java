package com.lol.ui.core;

import com.lol.core.util.InfoUtil;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.Objects.nonNull;

@Slf4j
public final class CalistoCondition {

    /**
     * Проверяет состояние при котором значение инпута не начнет совпадать с ожидаемым текстом
     *
     * @param by   - элемент для поиска
     * @param data - текст для ввода
     */
    public static ExpectedCondition<Boolean> keysSendCondition(final By by, final String data) {
        return new ExpectedCondition<>() {
            @Override
            public Boolean apply(@Nullable WebDriver driver) {
                try {
                    final var element = findElement(by);
                    if (!element.isDisplayed()) return false;

                    clearAndSendKeys(element, data);
                    return data.equals(element.getDomAttribute("value"));
                } catch (NoSuchElementException | StaleElementReferenceException e) {
                    return false;
                }
            }

            @Override
            public String toString() {
                return String.format("key condition not equivalent to %s", data);
            }
        };
    }

    private static void clearAndSendKeys(WebElement element, String data) {
        var value = element.getDomAttribute("value");
        if (nonNull(value) && !value.isEmpty()) {
            var selectAll = InfoUtil.isMac() ? Keys.COMMAND + "a" : Keys.CONTROL + "a";
            element.sendKeys(selectAll, Keys.DELETE);
        }
        element.sendKeys(data);
    }

    /**
     * Проверяет что вы не можете кликнуть на элемент
     *
     * @param locator used to find the element
     * @return true если элемент not clickable (visible and not enabled)
     */
    public static ExpectedCondition<Boolean> elementNotToBeClickable(final By locator) {
        return new ExpectedCondition<>() {
            @Override
            public Boolean apply(WebDriver driver) {
                try {
                    final var element = ExpectedConditions.visibilityOfElementLocated(locator).apply(driver);
                    return nonNull(element) && !element.isEnabled();
                } catch (StaleElementReferenceException e) {
                    return false;
                }
            }

            @Override
            public String toString() {
                return "element not to be clickable: " + locator;
            }
        };
    }

    /**
     * Проверяет состояние до тех пор, пока значение чекбокса не будет соответствовать ожиданию
     *
     * @param element  - элемент для поиска
     * @param selected - ожидаемое значение
     */
    public static ExpectedCondition<Boolean> elementSelectCheckboxState(final WebElement element, final boolean selected) {
        return new ExpectedCondition<>() {
            @Override
            public Boolean apply(WebDriver driver) {
                try {
                    if (element.isSelected() != selected) {
                        element.click();
                        return false;
                    } else {
                        return true;
                    }
                } catch (StaleElementReferenceException e) {
                    log.error("Can't set state for checkbox '{}'", element);
                }

                return false;
            }

            @Override
            public String toString() {
                return String.format("element found by %s to %s be selected", element, (selected ? "" : "not "));
            }
        };
    }

    /**
     * Проверяет позицию элемента, до тех пор, пока он не прекратит изменяться
     *
     * @param by - локатор для поиска
     */
    public static ExpectedCondition<Boolean> steadinessOfElementLocated(final By by) {
        return new ExpectedCondition<>() {
            @Override
            public Boolean apply(WebDriver driver) {
                try {
                    var element = findElement(by);
                    var pointX1 = element.getLocation().x;
                    var pointY1 = element.getLocation().y;

                    var pointX2 = element.getLocation().x;
                    var pointY2 = element.getLocation().y;

                    return pointX1 == pointX2 || pointY1 == pointY2;
                } catch (StaleElementReferenceException e) {
                    log.error("Can't get element steadiness '{}'", by);
                }

                return false;
            }

            @Override
            public String toString() {
                return String.format("element found by %s not stop animating", by);
            }
        };
    }

    public static ExpectedCondition<Boolean> cookieExist(final String data) {
        return new ExpectedCondition<>() {
            @Override
            public Boolean apply(@Nullable WebDriver input) {
                var cookies = Calisto.getWebDriver().manage().getCookies();
                return cookies.stream().anyMatch(cookie -> cookie.getName().equals(data));
            }

            @Override
            public String toString() {
                return String.format("cookie with '%s' doesn't exist", data);
            }
        };
    }

    public static ExpectedCondition<Boolean> cookiesExist(final Set<String> data) {
        return new ExpectedCondition<>() {
            @Override
            public Boolean apply(@Nullable WebDriver input) {
                var cookies = Calisto.getWebDriver().manage().getCookies();
                return cookies.stream().map(Cookie::getName).collect(Collectors.toSet()).containsAll(data);
            }

            @Override
            public String toString() {
                return String.format("cookie with '%s' doesn't exist", String.join(",", data));
            }
        };
    }

    public static ExpectedCondition<WebElement> visibilityOfElementLocated(final WebElement webElement, final By locator) {
        return new ExpectedCondition<>() {
            @Override
            public WebElement apply(WebDriver driver) {
                try {
                    return elementIfVisible(findElement(webElement, locator));
                } catch (NoSuchElementException | StaleElementReferenceException e) {
                    return null;
                }
            }

            @Override
            public String toString() {
                return "visibility of element located by " + locator;
            }
        };
    }

    public static ExpectedCondition<WebElement> presentsOfElementLocated(final WebElement webElement, final By locator) {
        return new ExpectedCondition<>() {
            @Override
            public WebElement apply(WebDriver driver) {
                try {
                    return findElement(webElement, locator);
                } catch (NoSuchElementException | StaleElementReferenceException e) {
                    return null;
                }
            }

            @Override
            public String toString() {
                return "Presents of element located by " + locator;
            }
        };
    }

    public static ExpectedCondition<Boolean> visibilityOfElementLocated(final By locator) {
        return new ExpectedCondition<>() {
            @Override
            public Boolean apply(WebDriver driver) {
                try {
                    return driver.findElement(locator).isDisplayed();
                } catch (NoSuchElementException | StaleElementReferenceException e) {
                    // Returns false because the element is not present in DOM. The
                    // try block checks if the element is present and visible.
                    return false;
                }

            }

            @Override
            public String toString() {
                return "element is visible: " + locator;
            }
        };
    }

    public static ExpectedCondition<Boolean> invisibilityOfElementLocated(final WebElement webElement, final By locator) {
        return new ExpectedCondition<>() {
            @Override
            public Boolean apply(WebDriver driver) {
                try {
                    return !(findElement(webElement, locator).isDisplayed());
                } catch (NoSuchElementException | StaleElementReferenceException e) {
                    // Returns true because the element is not present in DOM. The
                    // try block checks if the element is present but is invisible.
                    // Returns true because stale element reference implies that element
                    // is no longer visible.
                    return true;
                }
            }

            @Override
            public String toString() {
                return "element to no longer be visible: " + locator;
            }
        };
    }

    public static ExpectedCondition<WebElement> elementToBeClickable(final WebElement webElement, final By locator) {
        return new ExpectedCondition<>() {
            @Override
            public WebElement apply(WebDriver driver) {
                try {
                    final var element = visibilityOfElementLocated(webElement, locator).apply(driver);
                    if (nonNull(element) && element.isEnabled()) {
                        return element;
                    }
                    return null;
                } catch (NoSuchElementException | StaleElementReferenceException e) {
                    return null;
                }
            }

            @Override
            public String toString() {
                return "element not to be clickable: " + locator;
            }
        };
    }

    public static ExpectedCondition<Boolean> textToBePresentInAttributeLocated(final By locator,
                                                                               final String attribute,
                                                                               final String text) {
        return new ExpectedCondition<>() {
            @Override
            public Boolean apply(WebDriver driver) {
                try {
                    var elementText = driver.findElement(locator).getDomAttribute(attribute);
                    return nonNull(elementText) && elementText.contains(text);
                } catch (NoSuchElementException | StaleElementReferenceException e) {
                    return false;
                }
            }

            @Override
            public String toString() {
                return String.format("text ('%s') to be present in element found by %s",
                        text, locator);
            }
        };
    }

    public static ExpectedCondition<List<WebElement>> elementCollectionPresent(By locator) {
        return elementCollectionPresent(locator, null);
    }

    public static ExpectedCondition<List<WebElement>> elementCollectionPresent(final By locator, final WebElement webElement) {
        return new ExpectedCondition<>() {
            @Override
            public List<WebElement> apply(WebDriver driver) {
                try {
                    final List<WebElement> webElements = (nonNull(webElement) ? webElement : driver).findElements(locator);
                    if (!webElements.isEmpty()) {
                        return webElements;
                    }
                    throw new NoSuchElementException("Elements size < 1");
                } catch (StaleElementReferenceException e) {
                    return null;
                }
            }

            @Override
            public String toString() {
                return String.format("element collection located at %s", locator);
            }
        };
    }

    public static ExpectedCondition<Boolean> elementCollectionNotPresent(final By locator) {
        return new ExpectedCondition<>() {
            @Override
            public Boolean apply(WebDriver driver) {
                try {
                    final List<WebElement> webElements = driver.findElements(locator);
                    return webElements.isEmpty();
                } catch (NoSuchElementException | StaleElementReferenceException e) {
                    return true;
                }
            }

            @Override
            public String toString() {
                return String.format("element collection not located at %s", locator);
            }
        };
    }

    private static WebElement findElement(final By by) {
        try {
            return Calisto.getWebDriver().findElements(by).stream().findFirst().orElseThrow(
                    () -> new NoSuchElementException("Cannot locate an element using " + by));
        } catch (NoSuchElementException e) {
            throw e;
        } catch (WebDriverException e) {
            log.warn(String.format("WebDriverException thrown by findElement(%s)", by), e);
            throw e;
        }
    }

    private static WebElement findElement(final WebElement webElement, final By by) {
        try {
            return webElement.findElements(by).stream().findFirst().orElseThrow(
                    () -> new NoSuchElementException("Cannot locate an element using " + by));
        } catch (NoSuchElementException e) {
            throw e;
        } catch (WebDriverException e) {
            log.warn(String.format("WebDriverException thrown by findElement(%s)", by), e);
            throw e;
        }
    }

    private static WebElement elementIfVisible(WebElement element) {
        return element.isDisplayed() ? element : null;
    }
}
