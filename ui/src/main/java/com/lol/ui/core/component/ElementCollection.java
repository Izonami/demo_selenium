package com.lol.ui.core.component;

import com.lol.ui.core.ByCalisto;
import com.lol.ui.core.Calisto;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

import static java.util.Objects.isNull;

@ToString(callSuper = true)
@Slf4j
public final class ElementCollection extends CollectionComponent {

    public ElementCollection(final By by, final String description) {
        super(by, description);
    }

    public ElementCollection(final By by, final long timeout, final String description) {
        super(by, timeout, description);
    }

    @Override
    protected List<WebElement> getComponents() {
        log.debug("getComponent {} with locator {}", getDescription(), getBy());
        return shouldBe().elementsExists();
    }

    private List<WebElement> getComponents(final Object... args) {
        setBy(ByCalisto.xpathExpression(((ByCalisto)getBy()).getDefaultXpathExpression(), args));
        log.debug("Get {}'s with locator {}", getClass().getSimpleName(), getBy());
        if (isNull(components)) {
            components = shouldBe().elementsExists();
        }
        return components;
    }

    public void clickOnElementWithText(final String text) {
        for (final var we : getComponents()) {
            final var elementText = we.getText();
            if (elementText.contains(text)) {
                log.debug("Click on element {} with text {}", we, elementText);
                we.click();
                break;
            }
        }
    }

    public String getElementText(final int order) {
        final var elements = getComponents();
        if (elements.size() > order) {
            return elements.get(order).getText();
        } else {
            log.error("Try to get text from {} element", order);
            return "element_not_found";
        }
    }

    public void moveOnElementWithText(final String text) {
        for (final var we : getComponents()) {
            final var elementText = we.getText();
            if (elementText.contains(text)) {
                log.debug("Hover on element {} with text {}", we, elementText);
                Calisto.action().moveToElement(we).perform();
                break;
            }
        }
    }

    public Boolean isElementWithTextPresent(final String text) {
        for (final var we : getComponents()) {
            final var elementText = we.getText();
            if (elementText.contains(text)) {
                log.debug("Element {} contains text {}", we, elementText);
                return true;
            }
        }
        return false;
    }

    public int elementCount() {
        return getComponents().size();
    }
}
