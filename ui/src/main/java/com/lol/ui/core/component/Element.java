package com.lol.ui.core.component;

import com.lol.ui.core.ByCalisto;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

@ToString(callSuper = true)
@Slf4j
public final class Element extends AbstractComponent {

    public Element(final By by, final String description) {
        super(by, description);
    }

    public Element(final By by, final long timeout, final String description) {
        super(by, timeout, description);
    }

    @Override
    public WebElement getComponent() {
        log.debug("getComponent {} with locator {}", getDescription(), getBy());
        return shouldBe().visible();
    }

    public synchronized WebElement getComponent(final Object... args) {
        setBy(ByCalisto.xpathExpression(((ByCalisto) getBy()).getDefaultXpathExpression(), args));
        return getComponent();
    }

    public synchronized void click(final Object... args) {
        setBy(ByCalisto.xpathExpression(((ByCalisto) getBy()).getDefaultXpathExpression(), args));
        click();
    }

    public void click() {
        log.debug("Click {} with locator {}", getDescription(), getBy());
        getComponent().click();
    }

    public synchronized String getText(final Object... args) {
        setBy(ByCalisto.xpathExpression(getLocator(), args));
        return getText();
    }

    public String getText() {
        final var text = getComponent().getText();
        log.debug("Get text '{}' for {} with locator {}", text, getDescription(), getBy());
        return text;
    }

    public String getAttribute(final String attr) {
        final var attributeValue = getComponent().getAttribute(attr);
        log.debug("Get text '{}' for {} with locator {}", attributeValue, getDescription(), getBy());
        return attributeValue;
    }
}
