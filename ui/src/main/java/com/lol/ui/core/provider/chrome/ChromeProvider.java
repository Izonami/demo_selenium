package com.lol.ui.core.provider.chrome;

import com.lol.ui.core.config.BrowserProperties;
import org.testng.Reporter;

import java.util.Map;

public final class ChromeProvider extends AbstractChromeProvider {

    @Override
    public void createDriver(final String version) {
        super.createDriver(version);
        options.setCapability("moon:options", Map.of(
                "name", Reporter.getCurrentTestResult().getName(),
                "enableVideo", BrowserProperties.VIDEO,
                "sessionTimeout", "5m",
                "screenResolution", "1920x1080x24",
                "hostsEntries", BrowserProperties.HOSTS_ENTRIES
        ));

        createRemoteDriver(options);
    }
}
