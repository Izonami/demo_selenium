package com.lol.ui.google;

import com.lol.ui.core.page.Router;
import com.lol.ui.google.page.home.HomePage;

public final class GoogleRouter extends Router {

    public static HomePage home() {
        return (HomePage) getPage(HomePage.class);
    }
}
