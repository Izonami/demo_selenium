package com.lol.ui.google.page.home;

import com.lol.ui.core.page.Window;
import com.lol.ui.google.GooglePage;
import io.qameta.allure.Step;

public final class HomePage implements GooglePage, Window, HomeCheck {

    @Step("Нажать на кнопку мне повезет")
    public void clickOnLucky() {
        iWillBeLucky.click();
    }

    @Override
    public String pageUrl() {
        return "";
    }
}
