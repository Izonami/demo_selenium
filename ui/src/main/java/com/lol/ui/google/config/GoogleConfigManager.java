package com.lol.ui.google.config;

import com.lol.core.config.AbstractConfigManager;

public final class GoogleConfigManager extends AbstractConfigManager {

    private static final String CONFIG_DIR = "config";

    @Override
    public void loadConfig() {
        ENGINE.loadConfig(UiProperties.class, UiProperties.NAME, CONFIG_DIR);
    }

    private GoogleConfigManager() {}

    public static GoogleConfigManager getInstance() {
        return Singleton.INSTANCE;
    }

    private static class Singleton {
        private static final GoogleConfigManager INSTANCE = new GoogleConfigManager();
    }
}
