package com.lol.ui.google;

import com.lol.ui.core.Calisto;
import com.lol.ui.core.page.Page;
import com.lol.ui.google.config.UiProperties;

public interface GooglePage extends Page {

    default void goToPage() {
        goToPage(pageUrl());
    }

    default void goToPage(final String url) {
        Calisto.open(UiProperties.URL + url);
    }
}
