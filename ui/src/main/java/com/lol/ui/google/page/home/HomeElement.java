package com.lol.ui.google.page.home;

import com.lol.ui.core.component.Button;
import org.openqa.selenium.By;

public interface HomeElement {

    Button iWillBeLucky = new Button(By.xpath("(//div/center/input[@name='btnI'])[2]"), "кнопка 'Мне повезет'");
}
