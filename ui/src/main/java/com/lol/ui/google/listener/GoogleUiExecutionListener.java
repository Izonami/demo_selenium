package com.lol.ui.google.listener;

import com.lol.core.listener.ExecutionListener;
import com.lol.core.util.AllureUtil;
import com.lol.ui.core.config.CoreConfigManager;
import com.lol.ui.google.config.GoogleConfigManager;
import com.lol.ui.google.config.UiProperties;

import java.util.Map;

public final class GoogleUiExecutionListener extends ExecutionListener {

    public GoogleUiExecutionListener() {
        super(CoreConfigManager.getInstance(), GoogleConfigManager.getInstance());
    }

    @Override
    public void onExecutionStart() {
        super.onExecutionStart();
    }

    @Override
    public void setupAllureReport() {
        AllureUtil.allureEnvironmentWriter(Map.of(
                "BASE URL", UiProperties.URL
        ));
    }

    @Override
    public void onExecutionFinish() {
        super.onExecutionFinish();
        // Тут может быть код для очистки окружения после прогона тестов
    }
}
