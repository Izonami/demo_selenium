package com.lol.ui.google.config;

import com.lol.core.config.Config;

public final class UiProperties {

    public static final String NAME = "ui";

    @Config(configName = NAME, fieldName = "url", defaultValue = "https://google.com", env = "URL")
    public static String URL;
}
