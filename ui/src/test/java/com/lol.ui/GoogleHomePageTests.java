package com.lol.ui;

import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import org.testng.annotations.Test;

import static com.lol.ui.google.GoogleRouter.home;

@Epic("Главная страница")
@Feature("Действия с кнопками")
public final class GoogleHomePageTests {

    @Test(description = "Проверка открытия страницы doodles при клике на кнопку 'Мне повезет'", groups = Group.REGRESSION)
    public void testClickOnButton() {
        home().goToPage();
        home().clickOnLucky();
        home().checkPageContains("doodles");
    }
}
