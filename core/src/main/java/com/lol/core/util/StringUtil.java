package com.lol.core.util;

import lombok.experimental.UtilityClass;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

@UtilityClass
public class StringUtil {

    public static boolean isEmpty(final String value) {
        return isNull(value) || value.isEmpty() || value.isBlank();
    }

    public static boolean notEmpty(final String value) {
        return nonNull(value) && !value.isEmpty() && !value.isBlank();
    }
}
