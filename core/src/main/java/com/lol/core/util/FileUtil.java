package com.lol.core.util;

import lombok.extern.slf4j.Slf4j;

import java.io.InputStream;
import java.util.Objects;

@Slf4j
public final class FileUtil {

    public static InputStream getConfig(final String configName, final Class<?> configClass) {
        return Objects.requireNonNull(configClass.getClassLoader().getResourceAsStream(configName));
    }
}
