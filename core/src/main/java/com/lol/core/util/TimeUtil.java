package com.lol.core.util;

import lombok.experimental.UtilityClass;

@UtilityClass
public class TimeUtil {

    public static String getReadableTime(final Long nanos) {
        final long tempSec = nanos / (1000 * 1000 * 1000);
        final long sec = tempSec % 60;
        final long min = (tempSec /60) % 60;
        return String.format("%dm %ds", min, sec);
    }
}
