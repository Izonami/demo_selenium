package com.lol.core.allure;

public enum CurrentStage {

    BEFORE,
    TEST,
    AFTER
}
