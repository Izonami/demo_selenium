package com.lol.core.appender;

import ch.qos.logback.core.OutputStreamAppender;

public class SimpleAppender<E> extends OutputStreamAppender<E> {

    @Override
    public void start() {
        setOutputStream(LogbackLogBuffer.getInstance());
        super.start();
    }
}
