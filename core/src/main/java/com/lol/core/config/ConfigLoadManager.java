package com.lol.core.config;

import java.util.ArrayList;
import java.util.List;

public final class ConfigLoadManager implements ILoadConfig {

    private final List<ILoadConfig> loadConfigList = new ArrayList<>();

    @Override
    public void loadConfig() {
        loadConfigList.forEach(ILoadConfig::loadConfig);
    }

    public void addToLoad(final ILoadConfig loadConfig) {
        loadConfigList.add(loadConfig);
    }
}
