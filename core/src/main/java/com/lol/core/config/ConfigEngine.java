package com.lol.core.config;

import com.lol.core.util.FileUtil;
import lombok.extern.slf4j.Slf4j;

import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.nio.charset.StandardCharsets;

import static com.lol.core.util.StringUtil.isEmpty;
import static java.util.Objects.nonNull;

@Slf4j
public final class ConfigEngine {

    private static final ExProperties PROPERTIES = new ExProperties();

    /**
     * Считывает данные из файла и проверяет наличие в классе полей аннотированных {@link Config}
     * после чего через рефлексию получает к ним доступ и задает значение в соответствии с данными полученными
     * из конфигурационного файла, в случае если в конфигурационном файле, не была найдена пара ключ=значение
     * соответствующая данным из аннотации {@link Config} значение для этой переменной задается из указанного
     * дефолтного значение {@link Config#defaultValue()}
     * В случае если файл не был найден, выпадает {@link Exception}
     * В случае если не удалось получить значение из файла, бросается {@link NumberFormatException}
     * В качестве аргументов получает:
     *
     * @param configClass - класс в котором ищутся поля аннотированные {@link Config}
     * @param configName  - название конфигурационного файла
     */
    public void loadConfig(final Class<?> configClass, final String configName, final String configDir) {
        final var filePath = configDir + "/" + configName + ".properties";
        try (final var lnr = new LineNumberReader(new InputStreamReader(FileUtil.getConfig(filePath, configClass), StandardCharsets.UTF_8))) {
            PROPERTIES.load(lnr);

            for (final var field : configClass.getDeclaredFields()) {
                Config annotation;
                if (nonNull((annotation = field.getAnnotation(Config.class)))) {
                    field.setAccessible(true);

                    final var fieldName = annotation.fieldName();
                    final var type = field.getType().getSimpleName().toLowerCase();
                    var args = System.getProperty(annotation.args(), "");
                    final var env = System.getenv(annotation.env());
                    var defaultValue = annotation.defaultValue();

                    //env имеет высший приоритет
                    if (nonNull(env)) {
                        args = env;
                    }

                    try {
                        switch (type) {
                            case "boolean":
                                final var defaultBool = isEmpty(defaultValue) ? "false" : defaultValue;
                                field.setBoolean(null, PROPERTIES.getBooleanProperty(fieldName, defaultBool, args));
                                break;
                            case "byte":
                                final var defaultByte = isEmpty(defaultValue) ? "0" : defaultValue;
                                field.setByte(null, PROPERTIES.getByteProperty(fieldName, defaultByte, args));
                                break;
                            case "int":
                                final var defaultInt = isEmpty(defaultValue) ? "0" : defaultValue;
                                field.setInt(null, PROPERTIES.getIntProperty(fieldName, defaultInt, args));
                                break;
                            case "long":
                                final var defaultLong = isEmpty(defaultValue) ? "0" : defaultValue;
                                field.setLong(null, PROPERTIES.getLongProperty(fieldName, defaultLong, args));
                                break;
                            case "float":
                                final var defaultFloat = isEmpty(defaultValue) ? "0.0" : defaultValue;
                                field.setFloat(null, PROPERTIES.getFloatProperty(fieldName, defaultFloat, args));
                                break;
                            case "list":
                                final var defaultList = isEmpty(defaultValue) ? "empty" : defaultValue;
                                field.set(null, PROPERTIES.getListProperty(fieldName, defaultList, args));
                                break;
                            case "map":
                                final var defaultMap = isEmpty(defaultValue) ? "key:empty" : defaultValue;
                                field.set(null, PROPERTIES.getMapProperty(fieldName, defaultMap, args));
                                break;
                            case "double":
                                final var defaultDouble = isEmpty(defaultValue) ? "0.0" : defaultValue;
                                field.setDouble(null, PROPERTIES.getDoubleProperty(fieldName, defaultDouble, args));
                                break;
                            case "string":
                                final var value = PROPERTIES.getStringProperty(fieldName, defaultValue, args);
                                field.set(null, value);
                                break;
                            default:
                                log.debug("Unknown field type: " + field.getType().getSimpleName() + " field name: " + field.getName() + " config: " + configName + ".properties");
                                break;
                        }
                    } catch (Exception e) {
                        log.error("Failed to Load " + filePath + " file. Field: " + field.getName() + " " + e.getMessage());
                    }
                    log.debug(configName + ": set " + field.getName() + "{" + fieldName + "} = " + field.get(null));
                }
            }
        } catch (Exception e) {
            log.error("Failed to Load " + filePath + " file.", e);
        }
    }
}
