package com.lol.core.config;

import com.lol.core.util.StringUtil;

import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Objects.isNull;

/**
 * Расширяет базовый класс {@link Properties}
 * Получаем с помощью {@link Properties#getProperty(String)}
 * значение из конфигурационного файла и дополнительными методами парсим значение
 * из вернувшейся строки.
 */
public final class ExProperties extends Properties {

    public String getStringProperty(final String name, final String defaultValue, final String systemProperty) {
        return StringUtil.isEmpty(systemProperty) ? getProperty(name, defaultValue) : systemProperty;
    }

    public int getIntProperty(final String name, final String defaultValue, final String systemProperty) {
        return StringUtil.isEmpty(systemProperty) ? getIntProperty(name, defaultValue) : Integer.parseInt(systemProperty);
    }

    public int getIntProperty(final String name, final String defaultValue) {
        return getIntProperty(name, Integer.parseInt(defaultValue));
    }

    public int getIntProperty(final String name, final int defaultValue) {
        final String val = getProperty(name);
        return StringUtil.isEmpty(val) ? defaultValue : Integer.parseInt(val.trim());
    }

    public long getLongProperty(final String name, final String defaultValue, final String systemProperty) {
        return StringUtil.isEmpty(systemProperty) ? getLongProperty(name, defaultValue) : Long.parseLong(systemProperty);
    }

    public long getLongProperty(final String name, final String defaultValue) {
        return getLongProperty(name, Long.parseLong(defaultValue));
    }

    public long getLongProperty(final String name, final long defaultValue) {
        final String val = getProperty(name);
        return StringUtil.isEmpty(val) ? defaultValue : Long.parseLong(val.trim());
    }

    public byte getByteProperty(final String name, final String defaultValue, final String systemProperty) {
        return StringUtil.isEmpty(systemProperty) ? getByteProperty(name, defaultValue) : Byte.parseByte(systemProperty);
    }

    public byte getByteProperty(final String name, final String defaultValue) {
        return getByteProperty(name, Byte.parseByte(defaultValue));
    }

    public byte getByteProperty(final String name, final byte defaultValue) {
        final String val = getProperty(name);
        return StringUtil.isEmpty(val) ? defaultValue : Byte.parseByte(val.trim());
    }

    public boolean getBooleanProperty(final String name, final String defaultValue, final String systemProperty) {
        return StringUtil.isEmpty(systemProperty) ? getBooleanProperty(name, defaultValue) : Boolean.parseBoolean(systemProperty);
    }

    public boolean getBooleanProperty(final String name, final String defaultValue) {
        return getBooleanProperty(name, Boolean.parseBoolean(defaultValue));
    }

    public boolean getBooleanProperty(final String name, final boolean defaultValue) {
        final String val = getProperty(name);
        return StringUtil.isEmpty(val) ? defaultValue : Boolean.parseBoolean(val.trim());
    }

    public float getFloatProperty(final String name, final String defaultValue, final String systemProperty) {
        return StringUtil.isEmpty(systemProperty) ? getFloatProperty(name, defaultValue) : Float.parseFloat(systemProperty);
    }

    public float getFloatProperty(final String name, final String defaultValue) {
        return getFloatProperty(name, Float.parseFloat(defaultValue));
    }

    public float getFloatProperty(final String name, final float defaultValue) {
        final String val = getProperty(name);
        return StringUtil.isEmpty(val) ? defaultValue : Float.parseFloat(val.trim());
    }

    public double getDoubleProperty(final String name, final String defaultValue, final String systemProperty) {
        return StringUtil.isEmpty(systemProperty) ? getDoubleProperty(name, defaultValue) : Double.parseDouble(systemProperty);
    }

    public double getDoubleProperty(final String name, final String defaultValue) {
        return getDoubleProperty(name, Double.parseDouble(defaultValue));
    }

    public double getDoubleProperty(final String name, final double defaultValue) {
        final String val = getProperty(name);
        return StringUtil.isEmpty(val) ? defaultValue : Double.parseDouble(val.trim());
    }

    public List<?> getListProperty(final String name, final String defaultValue, final String systemProperty) {
        return StringUtil.isEmpty(systemProperty) ? getListProperty(name, defaultValue) : getList(systemProperty);
    }

    public List<?> getListProperty(final String name, final String defaultValue) {
        return getListProperty(name, getList(defaultValue));
    }

    public List<?> getListProperty(final String name, final List<?> defaultValue) {
        final String val = getProperty(name);
        return StringUtil.isEmpty(val) ? defaultValue : getList(val);
    }

    public Map<?, ?> getMapProperty(final String name, final String defaultValue, final String systemProperty) {
        return StringUtil.isEmpty(systemProperty) ? getMapProperty(name, defaultValue) : getMap(systemProperty);
    }

    public Map<?, ?> getMapProperty(final String name, final String defaultValue) {
        return getMapProperty(name, getMap(defaultValue));
    }

    public Map<?, ?> getMapProperty(final String name, final Map<?, ?> defaultValue) {
        final String val = getProperty(name);
        return StringUtil.isEmpty(val) ? defaultValue : getMap(val);
    }

    /**
     * Конвертирует строку в List
     * Разделителем между значениями выступает запятая
     * @param value - VALUE,VALUE_2,
     * @return - возвращает List
     */
    private List<?> getList(final String value) {
        return Stream.of(value.split(",")).map(String::trim).collect(Collectors.toList());
    }

    /**
     * Конвертирует строку в Map
     * Разделителем между ключом и значением выступает - :
     * а разделителем между парами - ;
     * @param value - KEY:VALUE;KEY_2:VALUE_2;
     * @return - возвращает Map
     */
    private Map<?, ?> getMap(final String value) {
        return Stream.of(value.split(";")).map(s -> s.split(":")).collect(Collectors.toMap(s -> s[0], s -> s[1]));
    }
}
