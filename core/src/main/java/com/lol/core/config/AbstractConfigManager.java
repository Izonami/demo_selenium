package com.lol.core.config;

public abstract class AbstractConfigManager implements ILoadConfig {

    protected static final ConfigEngine ENGINE = new ConfigEngine();

    public abstract void loadConfig();
}
