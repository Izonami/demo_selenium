package com.lol.core.listener;

import com.lol.core.config.ConfigLoadManager;
import com.lol.core.config.ILoadConfig;
import com.lol.core.util.TimeUtil;
import lombok.extern.slf4j.Slf4j;
import org.testng.IExecutionListener;

import java.util.Arrays;

@Slf4j
public abstract class ExecutionListener implements IExecutionListener {

    private static final long start = System.nanoTime();
    private final ConfigLoadManager loadManager = new ConfigLoadManager();

    public ExecutionListener(final ILoadConfig... loadConfig) {
        Arrays.stream(loadConfig).forEach(loadManager::addToLoad);
    }

    @Override
    public void onExecutionStart() {
        log.debug("Load Core config");
        loadManager.loadConfig();
        log.debug("Setup Allure report");
        setupAllureReport();
    }

    public abstract void setupAllureReport();

    @Override
    public void onExecutionFinish() {
        log.debug("All tests finished for {}", TimeUtil.getReadableTime(System.nanoTime() - start));
        // Тут может быть код для очистки окружения после прогона тестов
    }
}
