### 1. Установка окружения
#### 1.1 Установка Homebrew
Если homebrew уже установлен, можно перейти к следующему пункту. 

Для тех у кого его нет, необходимо к терминале выполнить команду

`/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"`

Далее устанавливаем расширение `cask` для `brew` необходимое для упрощения и повышения скорости установки

```
brew tap homebrew/cask-versions
brew update
brew tap homebrew/cask
```

#### 1.2 Установка Java
При наличии установленного Homebrew: 

```
brew tap adoptopenjdk/openjdk
brew install adoptopenjdk11 --cask
```
Для ОС Windows необходимо скачать и установить версию jdk11 для вашей версии со страницы 
https://www.oracle.com/java/technologies/javase/jdk11-archive-downloads.html

##### 1.2.1 Переключение версий Java

```
export JAVA_11_HOME=$(/usr/libexec/java_home -v11) -- задает перменную
alias java11='export JAVA_HOME=$JAVA_11_HOME' -- создаем алиас для быстрого переключения
```
Перезагружаем профиль для применения переменных

`source ~/.zshrc`

или bash_profile если вы старовер

`source ~/.bash_profile`

Выбирает текущей версией Java11

`java11`

Проверяем текущую версию Java

`java -version`

```
Если необходимо две версии то общий процесс будет выглядеть следующим образом 

brew install adoptopenjdk/openjdk/adoptopenjdk8 --cask
brew install adoptopenjdk11 --cask

export JAVA_8_HOME=$(/usr/libexec/java_home -v8)
export JAVA_11_HOME=$(/usr/libexec/java_home -v11)

alias java8='export JAVA_HOME=$JAVA_8_HOME'
alias java11='export JAVA_HOME=$JAVA_11_HOME'

source ~/.zshrc

java8 
java -version

java11
java -version
```

#### 1.3 Установка IDE
Переходим на страницу 

`https://www.jetbrains.com/ru-ru/idea/download/#section=mac`

для OC Windows, соответственно:

`https://www.jetbrains.com/ru-ru/idea/download/#section=windows`

скачиваем `Community Edition` и проходим обычный процесс установки

#### 2. Настройка среды разработки
После запуска жмем `Import Project` и в диалоге указываем файл `build.gradle` в папке `dev/automag`

Устанавливаем кодировку в настройках `(cmd+,) Editor → File Encodings`
```
Global Encoding = UTF-8
Project Encoding = UTF-8
Default encoding for properties → ставим галочку на Transparent native-to-ascii conversion → вибираем UTF-8
```
#### 3. Клонирование репозитория

#### 4. Настройка проекта
### 5. Запуск тестов
### 6. Создание тестов
#### 6.1 Работа с аннотациями
Для соответствия аллюр-отчёта (раздел Behaviors) структуре сьютов TMS в тестах необходимо использовать аннотации `@Epic`,
`@Feature`, `@Store`.

Например, если в TMS кейс находится в сьюте "Главная страница" -> "Структура" -> "Шапка" для него должны быть указаны  
`@Epic("Главная страница")`  
`@Feature("Структура")`  
`@Story("Шапка")`

Если глубина вложенности кейса в TMS более 3-х уровней, все нижние сьюты указываются через " | " в аннотации `@Story`,
например `@Story("Шапка | Desktop Web")`

В `description` аннотации `@Test` дословно указывается название тест-кейса из TMS